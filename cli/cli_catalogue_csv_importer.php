<?php
// Free spech https://github.com/Behat/Behat/blob/master/bin/behat
if (is_file($autoload = getcwd() . '/vendor/autoload.php')) {
    require $autoload;
}

if (is_file($autoload = __DIR__ . '/../vendor/autoload.php')) {
    require($autoload);
} elseif (is_file($autoload = __DIR__ . '/../../../autoload.php')) {
    require($autoload);
} else {
    fwrite(
        STDERR,
        'You must set up the project dependencies, run the following commands:'.PHP_EOL.
        'curl -s http://getcomposer.org/installer | php'.PHP_EOL.
        'php composer.phar install'.PHP_EOL
    );
    exit(1);
}
use Dotenv\Dotenv;

// We use dotenv to load the configuration:
$dotenv = new Dotenv(dirname(__DIR__));
$dotenv->load();

// finished loader section.

// begin file reading:
$supplierFile = getenv('CSVFILE');
$dataModel = file_get_contents(getenv('DATAMODEL'));

// Transform the csv file into array:
$csv = array_map('str_getcsv', file($supplierFile));

// read every single line and unset invalid rows:
foreach ($csv as $a => $value) {
    if (count($value) === count($csv[0])) {
        // Set everything to uppercase:
        $csv[$a] = array_change_key_case(array_combine($csv[0], $value), CASE_UPPER);
    } else {
        unset($csv[$a]);
    }
};
array_shift($csv); # remove column header

// ended file reading.

$v1Products = array();
$sa24Products = array();

// Product manipulation:

foreach ($csv as $v1Product) {
    $replace = array(
        "%NAME%" => trim($v1Product['NAME']),
        "%GTIN%" => trim($v1Product['GTIN']),
        "%GLN%" => trim($v1Product['SUPPLIER_GLN']),
        "%REORDERPOINT%" => trim($v1Product['REORDER_POINT']),
        "%PRICE%" => trim($v1Product['PRICE']),
        "%MINSTOCK%" => (isset($v1Product['MIN_STOCK']) ? trim($v1Product['MIN_STOCK']) : '0'),
        "%NOTIFICATIONDAYS%" => (isset($v1Product['NOTIFICATION_DAYS']) ? trim($v1Product['NOTIFICATION_DAYS']) : '0'),
        "%LEADTIME%" => (isset($v1Product['LEAD_TIME']) ? trim($v1Product['LEAD_TIME']) : '0'),
    );

    $sa24ProductStr = strtr($dataModel, $replace);

    $sa24ProductArr = json_decode($sa24ProductStr);
    array_push($sa24Products, $sa24ProductArr);
}
persist($sa24Products);

// Product persistance:
function persist(array $sa24Products)
{
    // Mongo
    $server = getenv('MONGO_SERVER');
    $port = getenv('MONGO_PORT');
    $dbName = getenv('MONGO_DB_NAME');
    $manager = new MongoDB\Driver\Manager(
        "mongodb://" . $server . ':' . $port
    );
    $bw = new MongoDB\Driver\BulkWrite;

    try {
        foreach ($sa24Products as $document) {
            // as Array is better
            // convert to array recursively
            $document = json_decode(json_encode($document), true);
            // Document
            $document = ['catalogueItem' => $document];
            $document['insertTimestamp'] = time();
            $document['source'] = 'V1';
            // Filters
            $filters['catalogueItem.tradeItem.gtin'] = $document['catalogueItem']['tradeItem']['gtin'];
            // Upsert
            $bw->update(
                $filters,
                [
                    '$set' => $document
                ],
                [
                    'multi' => false,
                    'upsert' => true
                ]
            );
        }
        // DB name
        $mongoDBName = $dbName;
        // Collection name
        $mongoCollectionName = getenv('MONGO_COLLECTION_SA24_NAME');
        // executeBulkWrite
        $manager->executeBulkWrite("$mongoDBName.$mongoCollectionName", $bw);
        // stdOUT
        echo "Products inserted. \n\n\n";
    } catch (Exception $ex) {
        var_dump($ex);
    }
}
