# SA24 Import Catalogue CSV
SA24 Import Catalogue CSV
Provides csv product import into sa24.
Support PSR-1, PSR-2, PSR-3, PSR-4.

![sa24 package repository](https://www.dream-destination-wedding.com/images/exotic2.jpg)

## Install

Via Composer

``` bash
$ composer require tkl/data-import-catalogue-csv
```

## Usage

``` bash
$ php
$ 
```

## Testing

``` bash
$ phpunit
```

## Contributing

Please see [CONTRIBUTING](https://github.com/tkl/data-import-product-persister/blob/master/CONTRIBUTING.md) for details.

## Credits

- [Wilson Smith](https://github.com/wilsonsmith)
- [All Contributors](https://github.com/tkl/data-import-product-persister/contributors)

## License

The GNU General Public Licence version 3 (GPLv3). Please see [License File](LICENSE.md) for more information.
